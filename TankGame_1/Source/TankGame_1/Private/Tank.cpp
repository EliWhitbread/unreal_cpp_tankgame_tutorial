// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/World.h"
//#include "Public/TankBarrel.h"
#include "Projectile.h"
//#include "TankAimingComponent.h"
//#include "TankMovementComponent.h"
#include "Public/Tank.h"

// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//TankAimingComponent = CreateDefaultSubobject<UTankAimingComponent>(FName("Aiming Component"));
	//TankMovementComponent = CreateDefaultSubobject<UTankMovementComponent>(FName("Movement Component"));

}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = StartingHealth; 

}

// Called to bind functionality to input
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float ATank::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	int32 DamagePoints = FPlatformMath::RoundToInt(DamageAmount);
	int32 DamageToApply = FMath::Clamp(DamagePoints, 0, CurrentHealth); //why clamp?

	CurrentHealth -= DamageToApply;

	if (CurrentHealth <= 0)
	{
		OnDeath.Broadcast();
	}
	

	return DamageToApply;
}

float ATank::GetHealthPercent() const
{
	return (float)CurrentHealth / (float)StartingHealth;
}

//void ATank::AimAt(FVector HitLocation)
//{
//	if(!ensure(TankAimingComponent)) {return;}
//	TankAimingComponent->AimAt(HitLocation, ProjectileLaunchSpeed);	
//}

//void ATank::FirePrimary()
//{
//	//if(!ensure(Barrel)) {return;}
//
//	bool canFire = (GetWorld()->TimeSeconds - LastFireTime) >= ReloadTime;
//	if(!canFire) {return;}
//	
//
//	AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileBlueprint,
//			Barrel->GetSocketLocation(FName("FirePoint")),
//			Barrel->GetSocketRotation(FName("FirePoint"))
//	);
//
//	//Projectile->LaunchProjectile(ProjectileLaunchSpeed);
//	LastFireTime = GetWorld()->TimeSeconds;
//}

//void ATank::SetBarrelReference(UTankBarrel* BarrelToSet)
//{
//	if(BarrelToSet == nullptr) {return;}
//	TankAimingComponent->SetBarrelReference(BarrelToSet);
//	Barrel = BarrelToSet;
//}
//
//void ATank::SetTurretReference(UTankTurret* TurretToSet)
//{
//	if(TurretToSet == nullptr) {return;}
//	TankAimingComponent->SetTurretReference(TurretToSet);
//}


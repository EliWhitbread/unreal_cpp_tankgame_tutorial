// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "TankAimingComponent.h"
#include "Public/Tank.h"
#include "Public/TankAIController.h"

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();

	//ATank* PlayerTank = GetPlayerTank();
	PlayerTankRef = GetWorld()->GetFirstPlayerController()->GetPawn();
	if (!PlayerTankRef)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s: Can't find PlayerTank!"), *GetOwner()->GetName())
	}
	ControlledTankRef = GetPawn();
	if (!ControlledTankRef)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s: Can't find Self!"), *GetOwner()->GetName())
	}
}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (PlayerTankRef == nullptr) //check if player still in level
	{
		PlayerTankRef = GetWorld()->GetFirstPlayerController()->GetPawn();
	}
	if(!ensure(PlayerTankRef && ControlledTankRef)){return;}

	MoveToActor(PlayerTankRef, AcceptanceRadius);
	ControlledTankRef->FindComponentByClass<UTankAimingComponent>()->AimAt(PlayerTankRef->GetActorLocation());
	ControlledTankRef->FindComponentByClass<UTankAimingComponent>()->FirePrimary();
}

void ATankAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if(!InPawn) {return;}

	ATank* PossessedTank = Cast<ATank>(InPawn);

	PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnPossessedTankDeath);
}

void ATankAIController::OnPossessedTankDeath()
{
	if(!GetPawn()) {return;}

	GetPawn()->DetachFromControllerPendingDestroy();
}

//ATank* ATankAIController::GetPlayerTank() const
//{
//	auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
//	return Cast<ATank>(PlayerPawn);
//}
//
//ATank* ATankAIController::GetControlledTank() const
//{
//	return Cast<ATank>(GetPawn());
//}
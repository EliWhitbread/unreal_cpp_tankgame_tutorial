// Fill out your copyright notice in the Description page of Project Settings.

#include "Kismet\GameplayStatics.h"
#include "Public/TankBarrel.h"
#include "Public/TankTurret.h"
#include "Projectile.h"
#include "Public/TankAimingComponent.h"



// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UTankAimingComponent::Initialise(UTankBarrel* TankBarrel, UTankTurret* TankTurret)
{
	Barrel = TankBarrel;
	Turret = TankTurret;

}

void UTankAimingComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (FiringStatus == EFiringStatus::Reloading)
	{
		if ((GetWorld()->TimeSeconds - LastFireTime) >= ReloadTime)
		{
			FiringStatus = EFiringStatus::Aiming;
		}
	}

}

void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();

	LastFireTime = GetWorld()->TimeSeconds;
}

int UTankAimingComponent::GetRemainingAmmo() const
{
	return CurrentAmmo;
}

void UTankAimingComponent::AimAt(FVector WorldSpaceAimPoint)
{
	if(!ensure(Barrel)) {return;}

	FVector OutLaunchVelocity;
	FVector StartLocation = Barrel->GetSocketLocation(FName("FirePoint"));

	if (UGameplayStatics::SuggestProjectileVelocity(
		this,
		OutLaunchVelocity,
		StartLocation,
		WorldSpaceAimPoint,
		ProjectileLaunchSpeed,
		false,
		0,
		0,
		ESuggestProjVelocityTraceOption::DoNotTrace))
	{
		FVector AimVector = OutLaunchVelocity.GetSafeNormal();
		MoveBarrelTowards(AimVector);
		//UE_LOG(LogTemp, Warning, TEXT("%s is aiming at: %s"), *GetOwner()->GetName(), *AimVector.ToString());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Projectile Velocity Fail!"));
	}

	//UE_LOG(LogTemp, Warning, TEXT("%s is aiming at: %s, from: %s, with speed: %f"), *GetOwner()->GetName(), *WorldSpaceAimPoint.ToString(), *Barrel->GetComponentLocation().ToString(), ProjectileSpeed); //Test
}

void UTankAimingComponent::FirePrimary()
{
	if(CurrentAmmo <= 0){return;}

	if (FiringStatus == EFiringStatus::Locked)
	{
		if (!ensure(Barrel && ProjectileBlueprint)) { return; }

		bool canFire = (GetWorld()->TimeSeconds - LastFireTime) >= ReloadTime;
		if (!canFire) { return; }


		AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileBlueprint,
			Barrel->GetSocketLocation(FName("FirePoint")),
			Barrel->GetSocketRotation(FName("FirePoint"))
			);

		Projectile->LaunchProjectile(ProjectileLaunchSpeed);
		LastFireTime = GetWorld()->TimeSeconds;

		FiringStatus = EFiringStatus::Reloading;
		CurrentAmmo--;
	}
	
}

//void UTankAimingComponent::SetBarrelReference(UTankBarrel* BarrelToSet)
//{
//	Barrel = BarrelToSet;
//}
//
//void UTankAimingComponent::SetTurretReference(UTankTurret* TurretToSet)
//{
//	Turret = TurretToSet;
//}

void UTankAimingComponent::MoveBarrelTowards(FVector AimVector)
{
	if (!ensure(Barrel || Turret)) { return; }

	FRotator BarrelRotion = Barrel->GetForwardVector().Rotation();
	FRotator AimRotation = AimVector.Rotation();
	FRotator DeltaRotation = AimRotation - BarrelRotion;

	Barrel->Elevate(DeltaRotation.Pitch);
	float WantYaw = FMath::Abs(DeltaRotation.Yaw) <= 180.0f ? DeltaRotation.Yaw : -DeltaRotation.Yaw;
	Turret->Rotate(WantYaw);
	

	if(FiringStatus == EFiringStatus::Reloading){return;};

	if (BarrelRotion.Equals(AimRotation, 0.2f))
	{
		FiringStatus = EFiringStatus::Locked;
	}
	else
	{
		FiringStatus = EFiringStatus::Aiming;
	}
}


// Fill out your copyright notice in the Description page of Project Settings.


#include "Public/TankBarrel.h"


void UTankBarrel::Elevate(float RelativeSpeed)
{
	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1.0f, 1.0f);
	float ElevationChange = RelativeSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	float RawNewElevation = FMath::Clamp<float>(RelativeRotation.Pitch + ElevationChange, MinElevationDegrees, MaxElevationDegrees);
	

	SetRelativeRotation(FRotator(RawNewElevation, 0.0f, 0.0f));

	//UE_LOG(LogTemp, Warning, TEXT("%f : Barrel Elevate() called with speed: %f"), GetWorld()->GetTimeSeconds(), RelativeSpeed);
}


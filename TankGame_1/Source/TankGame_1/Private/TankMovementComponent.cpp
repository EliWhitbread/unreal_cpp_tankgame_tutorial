// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTrack.h"
#include "TankMovementComponent.h"

void UTankMovementComponent::Initialise(UTankTrack* TrackToSet_Left, UTankTrack* TrackToSet_Right)
{
	//if (!ensure(TrackToSet_Left || TrackToSet_Right)) {return;}

	LeftTrack = TrackToSet_Left;
	RightTrack = TrackToSet_Right;
}

void UTankMovementComponent::IntendMoveForward(float Throw)
{
	if (!ensure(LeftTrack)) { return; }
	if (!ensure(RightTrack)) { return; }

	LeftTrack->SetThrottle(Throw);
	RightTrack->SetThrottle(Throw);

}

void UTankMovementComponent::IntendTurnRight(float Throw)
{
	//if (!ensure(LeftTrack && RightTrack)) { return; }
	if (!ensure(RightTrack)) { return; }
	if (!ensure(LeftTrack)) { return; }
	

	LeftTrack->SetThrottle(Throw);
	RightTrack->SetThrottle(-Throw);

}

void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
	//No need to call Super - replacing all functionality.
	//UE_LOG(LogTemp, Warning, TEXT("%s Move Velocity: %s"), *GetOwner()->GetName(), *MoveVelocity.ToString());

	FVector TankForward = GetOwner()->GetActorForwardVector().GetSafeNormal();
	FVector AIForwardIntention = MoveVelocity.GetSafeNormal();
	float ForwardThrow = FVector::DotProduct(TankForward, AIForwardIntention);
	
	FVector AIRorationIntention = FVector::CrossProduct(TankForward, AIForwardIntention);
	IntendTurnRight(AIRorationIntention.Z);

	//if(AIRorationIntention.Z != 0.0f) {return;}

	IntendMoveForward(ForwardThrow);

}
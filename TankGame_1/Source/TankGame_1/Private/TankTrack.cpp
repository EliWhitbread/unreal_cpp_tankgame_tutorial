// Fill out your copyright notice in the Description page of Project Settings.


#include "TankTrack.h"

UTankTrack::UTankTrack()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UTankTrack::BeginPlay()
{
	Super::BeginPlay();

	RootComponent = Cast<UStaticMeshComponent>(GetOwner()->GetRootComponent());
	OnComponentHit.AddDynamic(this, &UTankTrack::OnHit);
}

//void UTankTrack::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
//{
//	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//
//	if(!ensure(RootComponent)) {return;}
//
//	float SlippageSpeed = FVector::DotProduct(GetRightVector(), GetComponentVelocity());
//	FVector CorrectionAcceleration = -SlippageSpeed / DeltaTime * GetRightVector();
//	FVector CorrectionForce = (RootComponent->GetMass() * CorrectionAcceleration) / 2;
//	RootComponent->AddForce(CorrectionForce);
//}

void UTankTrack::SetThrottle(float Throttle)
{
	CurrentThrottle = FMath::Clamp<float>(CurrentThrottle + Throttle, -1.0f, 1.0f );

	//UE_LOG(LogTemp, Warning, TEXT("%s %s Set track force to: %s"), *GetOwner()->GetName(), *GetName(), *ForceApplied.ToString());

}

void UTankTrack::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	DriveTrack();
	ApplySidewaysForce();
	CurrentThrottle = 0.0f;
}

void UTankTrack::DriveTrack()
{
	FVector ForceApplied = GetForwardVector() * CurrentThrottle * TrackMaxDrivingForce;
	FVector ForceLocation = GetComponentLocation();
	UPrimitiveComponent* TankRoot = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());
	if (!ensure(TankRoot))
	{
		UE_LOG(LogTemp, Warning, TEXT("%s No root component found"), *GetName());
		return;
	}
	TankRoot->AddForceAtLocation(ForceApplied, ForceLocation);
}

void UTankTrack::ApplySidewaysForce()
{
	if (!ensure(RootComponent)) { return; }

	float DTime = GetWorld()->GetTimeSeconds();
	float SlippageSpeed = FVector::DotProduct(GetRightVector(), GetComponentVelocity());
	FVector CorrectionAcceleration = -SlippageSpeed / DTime * GetRightVector();
	FVector CorrectionForce = (RootComponent->GetMass() * CorrectionAcceleration) / 2;
	RootComponent->AddForce(CorrectionForce);
}


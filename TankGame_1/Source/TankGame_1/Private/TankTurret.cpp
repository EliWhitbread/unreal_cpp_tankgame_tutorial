// Fill out your copyright notice in the Description page of Project Settings.


#include "Public/TankTurret.h"

void UTankTurret::Rotate(float RelativeRotationSpeed)
{
	RelativeRotationSpeed = FMath::Clamp<float>(RelativeRotationSpeed, -1.0f, 1.0f);
	float RotationChange = RelativeRotationSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	float RawNewRotation = RelativeRotation.Yaw + RotationChange;


	SetRelativeRotation(FRotator(0.0f, RawNewRotation, 0.0f));
}


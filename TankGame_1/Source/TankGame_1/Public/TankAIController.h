// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TankAIController.generated.h"

//forward dec
//class ATank;
/**
 * 
 */
UCLASS()
class TANKGAME_1_API ATankAIController : public AAIController
{
	GENERATED_BODY()

protected:

	//closest allowed distance to target tank
	UPROPERTY(EditAnywhere, Category = "Setup")
	float AcceptanceRadius = 7000.0f;

private:
	
	void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	virtual void SetPawn(APawn* InPawn) override;

	UFUNCTION()
	void OnPossessedTankDeath();

	//ATank* GetPlayerTank() const;
	//ATank* GetControlledTank() const;

	APawn* PlayerTankRef = nullptr;
	APawn* ControlledTankRef = nullptr;
	
	
};

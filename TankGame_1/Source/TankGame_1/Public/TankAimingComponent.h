// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"

UENUM()
enum class EFiringStatus : uint8
{
	Locked, Aiming, Reloading
};

class UTankBarrel; // Forward declaration
class UTankTurret;
class AProjectile;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TANKGAME_1_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialise(UTankBarrel* TankBarrel, UTankTurret* TankTurret);

protected:

	UPROPERTY(BlueprintReadOnly, Category = "State")
	EFiringStatus FiringStatus = EFiringStatus::Reloading;

public:	
	
	void AimAt(FVector WorldSpaceAimPoint);

	UFUNCTION(BlueprintCallable, Category = "Shooting")
	void FirePrimary();

	/*void SetBarrelReference(UTankBarrel* BarrelToSet);
	void SetTurretReference(UTankTurret* TurretToSet);*/
	UFUNCTION(BlueprintCallable, Category = "Shooting")
	int GetRemainingAmmo() const;

private:

	UTankAimingComponent();

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void MoveBarrelTowards(FVector AimVector);
	UTankBarrel* Barrel = nullptr;
	UTankTurret* Turret = nullptr;

	// ------

	UPROPERTY(EditAnywhere, Category = "Firing")
	float ProjectileLaunchSpeed = 10000.0f; //TODO - Find good default speed.

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	int32 CurrentAmmo = 3;

	UPROPERTY(EditDefaultsOnly, Category = "Firing") //EditDefaultsOnly allows setting only at prefab/BP level - no instance editing. EditAnywhere allows per-instance setting
	float ReloadTime = 3.0f;

	double LastFireTime = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<AProjectile> ProjectileBlueprint = nullptr;

	

};

// Fill out your copyright notice in the Description page of Project Settings.

//#include "Public/Tank.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Tank.h"
#include "TankAimingComponent.h"
#include "TankPlayerController.h"

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();

	UTankAimingComponent* AimComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if(!ensure(AimComponent)) { return;}

	FoundAimingComponent(AimComponent);
}

//void FoundAimingComponent(UTankAimingComponent* AimComponentRef){} //cpp def not required for BlueprintImplementableEvent

void ATankPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn) {
		ATank* PossessedTank = Cast<ATank>(InPawn);
		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankPlayerController::OnPossessedTankDeath);
	}
}

void ATankPlayerController::OnPossessedTankDeath()
{
	StartSpectatingOnly();
}

void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AimTowardCrosshair();
	//UE_LOG(LogTemp, Warning, TEXT("TankPlayerController... TICK!")); //Test
}

void ATankPlayerController::AimTowardCrosshair()
{
	if(!Cast<ATank>(GetPawn())) {return;}

	UTankAimingComponent* AimComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(AimComponent)) { return; }

	FVector HitLocation;
	if (GetAimRayHitLocation(HitLocation))
	{
		
		AimComponent->AimAt(HitLocation);
	}

}

bool ATankPlayerController::GetAimRayHitLocation(FVector& OutHitLocation) const
{
	//Find the crosshair position in screen pixel coordinates
	int32 VPSizeX, VPSizeY;
	GetViewportSize(VPSizeX, VPSizeY);
	FVector2D ScreenLocation = FVector2D(VPSizeX * CrossHairXLocation, VPSizeY * CrossHairYLocation);

	FVector LookDirection;

	if (GetLookDirection(ScreenLocation, LookDirection))
	{
		return GetLookVectorHitLocation(LookDirection, OutHitLocation);
		//UE_LOG(LogTemp, Warning, TEXT("Look Direction: %s"), *LookDirection.ToString()); //Test
	}

	return false;
}

bool ATankPlayerController::GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const
{
	FVector CamWorldLocation; // to be discarded - testing only
	return DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, CamWorldLocation, LookDirection);
}

bool ATankPlayerController::GetLookVectorHitLocation(FVector LookDirection, FVector& OutHitLocation) const
{
	FHitResult HitResult;
	FVector StartLocation = PlayerCameraManager->GetCameraLocation();
	FVector EndLocation = StartLocation + (LookDirection * TankLineTraceRange);
	if (GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility))
	{
		OutHitLocation = HitResult.Location;
		return true;
	}

	return false;
}



